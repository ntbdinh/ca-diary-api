const _ = require('lodash');
const bcrypt = require('bcrypt');
const express = require('express');
const CaDiaryCms  = require('ca-diary-cms');

const utils = require('../utils');
const StatusError = require('../utils/statusError');
const User = CaDiaryCms.models.User.model;

const app = module.exports = express.Router();

app.get('/api/user/hello', (req, res) => res.json('hello'));
app.post('/api/user/login', async (req, res, next) => {
  const email = _.toLower(req.body.email);
  const password = req.body.password;

  if (!email || !password) {
    next(StatusError.InvalidInput);
    return;
  }

  const user = await User.findOne({ email });
  if (!user) {
    next(StatusError.WrongEmailOrPwd);
    return;
  }

  const matchedPassword = await bcrypt.compare(password, user.password);
  if (!matchedPassword) {
    next(StatusError.WrongEmailOrPwd);
    return;
  }

  res.json({token: utils.createAccessToken(user.toObject())});
});
app.post('/api/user/logout', (req, res) => res.json('Bye'));
app.post('/api/user/register', (req, res) => res.json('account created successfully'));
app.post('/api/user/forgotPass', (req, res) => res.json('new password will be created'));

app.get('/api/profile/me', (req, res) => res.json('Cà profile'));
app.post('/api/profile/update', (req, res) => res.json('Update Cà profile'));
app.post('/api/profile/changePass', (req, res) => res.json('Password has been changed'));

app.get('/api/diary/all', (req, res) => res.json('Get all diary'));
app.get('/api/diary/info/{id}', (req, res) => res.json('Diary information'));
app.post('/api/diary/create', (req, res) => res.json('Diary has been created'));
app.post('/api/diary/attach', (req, res) => res.json('Attached'));
