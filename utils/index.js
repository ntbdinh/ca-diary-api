const _ = require('lodash');
const jwt = require('jsonwebtoken');

const config = require('../config.json');

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
    jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return jti;
}

module.exports.createAccessToken = function (user) {
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    permissions: user.permissions,
    profile: _.omit(user, 'password'),
    jti: genJti(), // unique identifier for the token
    alg: 'HS256',
  }, config.secret);
}