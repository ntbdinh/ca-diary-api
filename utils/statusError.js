function StatusError(msg, code, status) {
  const err = Error.call(this, msg);
  err.status = status;
  err.code = code;
  err.name = 'StatusError';
  return err;
}

StatusError.prototype = Object.create(Error.prototype, {
  constructor: {value: StatusError},
});

module.exports = StatusError;

// commons
module.exports.Unauthorized = new StatusError('Unauthorized', 'unauthorized', 401);
module.exports.InvalidInput = new StatusError('InvalidInput', 'bad-request', 400);
module.exports.InternalError = new StatusError('InternalError', 'internal-error', 500);
module.exports.NotImplemented = new StatusError('Not implemented yet', 'not-implemented', 404);

// Login
module.exports.InvalidLogin = new StatusError('InvalidLogin', 'invalid-login', 400);
module.exports.WrongEmailOrPwd = new StatusError('WrongEmailOrPwd', 'wrong-email-or-password', 400);
