const cors = require('cors');
const express = require('express');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

dotenv.config();

const app = express();

// Parsers
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

const apiRoutes = require('./routes');
app.use(apiRoutes);

app.use(function (err, req, res, next) {
  res.status(err.status).json({message: err.message, code: err.code});
});

const port = process.env.PORT || 3000;
const dbUri = process.env.MONGO_URI || 'mongodb://localhost:27017/ca-diary';

mongoose.Promise = Promise;
mongoose.connect(dbUri, {
  useNewUrlParser: true,
  useMongoClient: true
}).then(
  () => {
    console.log('DB connected: ' + dbUri);
  },
  error => console.error(error),
);

app.listen(port, () => console.log(`Server listening on port: ${port}`));
